package com.umc.ict1.ctrl;

import com.umc.ict1.domain.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by wlslyy0909 on 2017-04-07.
 */

@Mapper
@Component
public interface AccountMapper {
    List<Account> findAll();
    Account findOne(@Param("id") String id);

}
