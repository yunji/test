package com.umc.ict1.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wlslyy0909 on 2017-04-06.
 */
@Controller
public class TestController {

    @GetMapping("/")
    public  String index(HttpServletRequest request, HttpServletResponse response){
        return "index";
    }

}
