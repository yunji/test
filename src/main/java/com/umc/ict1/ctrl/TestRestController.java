package com.umc.ict1.ctrl;

import com.umc.ict1.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by wlslyy0909 on 2017-04-06.
 */

@RestController
@Slf4j
public class TestRestController {

    @Autowired
    public AccountMapper mapper;
    Account account = new Account();

    @GetMapping("/account")
    public List<Account> accountList(){ return mapper.findAll();}


    @GetMapping("/account/{id}")
    public Account account(@PathVariable String id){
        Account account = mapper.findOne(id);
        log.debug("account is {}", account);
        return account;
    }

}
