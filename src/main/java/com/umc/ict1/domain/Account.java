package com.umc.ict1.domain;

import lombok.Data;
import lombok.ToString;
import org.apache.ibatis.type.Alias;

/**
 * Created by wlslyy0909 on 2017-04-07.
 */
@Alias(value = "Account")
@Data
@ToString
public class Account {
    private String id;
    private String pass;
}